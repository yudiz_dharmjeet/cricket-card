import React from "react";
import PropTypes from "prop-types";
import dateFormat from "dateformat";
import Helmet from "react-helmet";
// import { useSelector } from "react-redux";

import "./CricketCard.scss";
import { davidMiller, teamDefaultPhoto, winTrophy } from "../../assets/images";

function CricketCard({ cardData }) {
  const card = cardData.data;

  console.log(card);

  const date = dateFormat(card.sport_event?.scheduled, "dS mmmm, yyyy");

  return (
    <div className="card">
      <Helmet>
        <meta charSet="utf-8" />
        <meta
          name="cricket-card"
          content="This is all about the previous match"
        />
        <title>Cricket-Card</title>
        <link rel="canonical" href="http:localhost:3000" />
      </Helmet>

      <div className="upper-card">
        <div className="upper-card-left">
          <h4>{card.sport_event.season.name}</h4>
          <h1>
            {card.sport_event.competitors[0].name} vs{" "}
            {card.sport_event.competitors[1].name}
          </h1>
        </div>
        <div className="upper-card-right">
          <h4>
            {card.sport_event.venue.name}, {card.sport_event.venue.city_name} |{" "}
            {date}
          </h4>
          <h3>{card.sport_event_status.match_result}</h3>
        </div>
      </div>
      <div className="lower-card">
        <div className="lower-card-left">
          <div className="lower-card-left-team">
            <div className="img_wrapper">
              <img src={teamDefaultPhoto} alt="Team Default Photo" />
            </div>
            <div className="team">
              <h1 className="team_abb">
                {card.sport_event.competitors[1].abbreviation}
              </h1>
            </div>
            <h1 className="score" id="lose">
              {card.sport_event_status.period_scores[0].display_score} (
              {card.sport_event_status.period_scores[0].display_overs} OV)
            </h1>
          </div>
          <div className="lower-card-left-team">
            <div className="img_wrapper">
              <img src={teamDefaultPhoto} alt="Team Default Photo" />
            </div>
            <div className="team">
              <h1 className="team_abb">
                {card.sport_event.competitors[0].abbreviation}
              </h1>
              <img src={winTrophy} alt="WinTrophy" />
            </div>
            <h1 className="score">
              {card.sport_event_status.period_scores[1].display_score} (
              {card.sport_event_status.period_scores[1].display_overs} OV)
            </h1>
          </div>
        </div>
        <div className="lower-card-middle"></div>
        <div className="lower-card-right">
          <div className="image_wrapper">
            <img src={davidMiller} alt="davidMiller" />
          </div>

          <div className="lower-card-right-desc">
            <h2>Player of the match</h2>
            <h1>{card.statistics.man_of_the_match[0].name}</h1>
          </div>
        </div>
      </div>
    </div>
  );
}

CricketCard.propTypes = {
  cardData: PropTypes.object,
};

export default CricketCard;
