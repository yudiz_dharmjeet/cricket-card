import React, { useEffect } from "react";
import { useDispatch } from "react-redux";

import "./App.scss";
import { CricketCard } from "./components";
import data from "./data/cricket.json";
import allActions from "./store/allActions";

function App() {
  const cardData = data;

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(allActions.MainAction.fetchData(data.data));
  }, []);

  return (
    <div className="app">
      <CricketCard cardData={cardData} />
    </div>
  );
}

export default App;
