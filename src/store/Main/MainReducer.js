import { FETCH_DATA } from "../action-types";

const initialState = {
  card: {},
};

const main = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return {
        ...state,
        card: action.payload,
      };

    default:
      return state;
  }
};

export default main;
