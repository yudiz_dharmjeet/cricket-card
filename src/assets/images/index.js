import winTrophy from "./win_trophy.png";
import davidMiller from "./david_miller.png";
import teamDefaultPhoto from "./team_default_photo.png";

export { winTrophy, davidMiller, teamDefaultPhoto };
